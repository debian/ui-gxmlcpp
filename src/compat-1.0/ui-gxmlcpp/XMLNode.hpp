/*
 * (C) Copyright 2002, Schlund+Partner AG
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/**
 * @file XMLNode.hpp
 * @version @$Id$
 * @author Schlund + Partner AG
 * @brief *ps*
 *
 * @bug getContent() should not throw if content is empty, but deliver an empty content.
 * @bug All get*() methods MUST be const.
 *
 * (C) Copyright by Schlund+Partner AG
 *
 * Synopsis: @#include <ui-gxmlcpp/XMLNode.hpp>
 *
 */
#ifndef UI_GXML_XMLNODE_HPP
#define UI_GXML_XMLNODE_HPP

// THIS IS A COMPAT API ONLY
#include <ui-gxmlcpp/compat_warning.h>

// STDC++
#include <string>

// C libraries
#include <libxslt/xsltInternals.h>
#include <libxml/xpath.h>
#include <libxml/tree.h>

// C++ libraries
#include <ui-gxmlcpp/Exception.hpp>

namespace UI {
namespace GXML {

class XMLNode
{
public:
	XMLNode();
	XMLNode(xmlNodePtr const node);
	~XMLNode();

	enum ErrorCode
	{
		NoNode_,        // Node pointer is NULL
		LastNode_,      // Node has no following sibling
		FirstNode_,     // Node has no preceding sibling
		NoParent_,      // Node has no parent node
		NoChild_,       // Node has no child node
		NoContent_,     // Node contains no text
		NoAttribute_,   // Node has no attributes
		BufferCreate_,  // Could not create XML buffer
		NodeDump_       // Could not dump the node
	};
	/** @brief Exceptions for this class. */
	typedef CodeException<ErrorCode> Exception;

	// XMLTree getDoc(); makes no sense till there is a doc-object
	////////////////////////////////////////////////////////////////////////
	// hint:
	// EVERY group of spcaes or characters is contained within a invisible
	// text-node.
	// i.e. <node/>  <node/> expands to <node/><text>  </text><node/>
	// return next node in order (every space between tags is a text node!)
	// throws LAST_NODE if no following sibling exists
	XMLNode getNext() const;
	// return previous node in order
	// throws FIRST_NODE if no preceding sibling exists
	XMLNode getPrev() const;
	// return parent of current node
	// throws DOC_NODE if no parent exists
	XMLNode getParent() const;
	// return first child of node (text-node if node has content)
	// throws
	XMLNode getChild() const;
	// return last child of node
	XMLNode getLast() const;

	// return node type (i.e. XML_ELEMENT_NODE, XML_ATTRIBUTE_NODE,
	// XML_TEXT_NODE, ... )
	int getType() const;
	// return name of node-tag
	std::string getName() const;
	// return content of node (which is the content of the first child text-node)
	std::string getContent() const;
	// return attribute value by name
	std::string getAttribute(std::string const & name) const;
	// XMLAttributeSet getAttributeSet( XMLNode* node );

	// returns XML subtree of node
	std::string getNodeDump() const;

	xmlNodePtr getNodePtr() const;

private:
	xmlNodePtr _node;
};

}}
#endif
