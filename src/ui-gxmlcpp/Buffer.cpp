/**
 * @file
 * @version @$Id$
 * @author
 * ui-gxmlcpp: C++ wrapper for Gnome libxml2/libxsl.
 * Copyright (C) Schlund+Partner AG 2002-2004.
 * Licensed under the GNU Lesser General Public License (LGPL). See
 * @ref License, or the file COPYING in distribution.
 */

// Local configuration
#include "config.h"

// Implementation
#include "Buffer.hpp"

// STDC++
#include <fstream>

// STDC
#include <cassert>
#include <cstring>

// C libraries
#include <ui-utilcpp/Text.hpp>

// Local
#include "Util.hpp"

namespace UI {
namespace GXML {

Buffer::Buffer()
{
	xmlBuffer * buf(xmlBufferCreate());
	if (!buf)
	{
		UI_THROW_CODE(Mem_, "xmlBuffer: Creation error");
	}
	set(buf);
}

Buffer::~Buffer()
{
	xmlBufferFree(get());
}

void Buffer::concat(std::string const & text)
{
	if (xmlBufferCCat(get(), text.c_str()) != 0)
	{
		UI_THROW_CODE(Mem_, "OutbutBuffer: Error concat'ing to buffer");
	}
}

char const * Buffer::getBuf() const
{
	return (char *) xmlBufferContent(get());
}

int Buffer::getSize() const
{
	return xmlBufferLength(get());
}


OutputBuffer::OutputBuffer()
{
#ifdef LIBXML2_NEW_BUFFER
	outbuf_ = xmlAllocOutputBuffer(0);
#else
	std::memset(&outbuf_, 0, (size_t) sizeof(xmlOutputBuffer));
	outbuf_.buffer = Buffer::get();
#endif
}

#ifdef LIBXML2_NEW_BUFFER

OutputBuffer::~OutputBuffer()
{
	xmlOutputBufferClose(outbuf_);
}

char const * OutputBuffer::getBuf() const
{
	return (char *) xmlOutputBufferGetContent(outbuf_);
}

int OutputBuffer::getSize() const
{
	return xmlOutputBufferGetSize(outbuf_);
}

xmlOutputBuffer * OutputBuffer::get() const
{
	return outbuf_;
}

xmlOutputBuffer * OutputBuffer::operator->() const
{
	return outbuf_;
}

#else

xmlOutputBuffer * OutputBuffer::get() const
{
	return (xmlOutputBuffer *) &outbuf_;
}

xmlOutputBuffer * OutputBuffer::operator->() const
{
	return (xmlOutputBuffer *) &outbuf_;
}
#endif

}}
