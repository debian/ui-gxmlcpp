/**
 * @file
 * @version @$Id$
 * @author
 * ui-gxmlcpp: C++ wrapper for Gnome libxml2/libxsl.
 * Copyright (C) Schlund+Partner AG 2002-2004.
 * Licensed under the GNU Lesser General Public License (LGPL). See
 * @ref License, or the file COPYING in distribution.
 */

// Local configuration
#include "config.h"

// Implementation
#include "XPathContext.hpp"

// STDC++
#include <string>
#include <cassert>

// C libraries
#include <libxml/xpathInternals.h>

// C++ libraries
#include <ui-utilcpp/Text.hpp>
#include <ui-gxmlcpp/Tree.hpp>

// Local
#include "Util.hpp"

namespace UI {
namespace GXML {

xmlXPathContext * XPathContext::create(xmlDoc * doc, xmlNode * node)
{
	xmlXPathContextPtr context(xmlXPathNewContext(doc));
	if (!context)
	{
		UI_THROW_CODE(Create_, "Could not create XPath context for tree");
	}
	context->node = node;
	return context;
}

XPathContext::XPathContext(xmlDoc * doc, xmlNode * node)
{
	set(create(doc, node));
}

XPathContext::XPathContext(Tree const & tree, xmlNode * node)
{
	set(create(tree.get(), node));
}

XPathContext::~XPathContext()
{
	xmlXPathFreeContext(get());
}

void XPathContext::registerNamespace(std::string const & prefix, std::string const & uri)
{
	::xmlXPathRegisterNs(get(), reinterpret_cast<xmlChar const *>(prefix.c_str()),
	                            reinterpret_cast<xmlChar const *>(uri.c_str()));
}

}}
