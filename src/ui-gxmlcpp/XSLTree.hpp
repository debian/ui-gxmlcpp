/**
 * @file
 * @version @$Id$
 * @author
 * ui-gxmlcpp: C++ wrapper for Gnome libxml2/libxsl.
 * Copyright (C) Schlund+Partner AG 2002-2004.
 * Licensed under the GNU Lesser General Public License (LGPL). See
 * @ref License, or the file COPYING in distribution.
 */
#ifndef UI_GXML_XSLTREE_HPP
#define UI_GXML_XSLTREE_HPP

// STDC++
#include <string>
#include <cassert>

// C++ Libraries
#include <ui-gxmlcpp/Tree.hpp>

// C libraries
#include <libxslt/xsltInternals.h>

namespace UI {
namespace GXML {

/** @brief XSL (stylesheet) Tree. */
class XSLTree: public Tree
{
private:
	static xsltStylesheetPtr create(xmlDocPtr const doc);

public:
	/** @brief Error codes for exceptions. */
	enum ErrorCode
	{
		Style_ = 0    // Not a valid stylesheet
	};
	/** @brief Exceptions for this class. */
	typedef CodeException<ErrorCode> Exception;

	/** @name Standard tree constructors.
	 * @{ @see UI::GXML::Tree. */
	XSLTree(char const * xml, int len=-1, std::string const & base=DefaultDocbase_, int const options=0);
	XSLTree(std::string const & xml, std::string const & base=DefaultDocbase_, int const options=0);
	XSLTree(std::istream & xml, std::string const & base=DefaultDocbase_, int const options=0);
	XSLTree(FileConstructor const dummy, std::string const & file, int const options=0);
	/** @} */

private:
	/** @name Not implemented -- protect against segfaults.
	 * @{ */
	XSLTree(XSLTree const & tree);
	XSLTree & operator=(XSLTree const & xslTree);
	/** @} */

public:
	~XSLTree();

	/** @brief Get output charset encoding (i.e., what is specified in xsl::output); empty if none is specified. */
	std::string getOutputEncoding() const;

protected:
	/** @brief We need to access libxml2 doc pointer to parse the stylesheet with libxml2 function. */
	friend class XSLTransTree;
	/** @brief Get libxml2 stylesheet struct. */
	xsltStylesheet * getStylesheet() const;

private:
	xsltStylesheetPtr style_;
};

}}
#endif
