/**
 * @file
 * @version @$Id$
 * @brief Include this to use the namespace shortcuts.
 */
#ifndef UI_GXML_SHORTCUTS_HPP
#define UI_GXML_SHORTCUTS_HPP

namespace UI { namespace GXML {}}

namespace X = UI::GXML;

#endif
