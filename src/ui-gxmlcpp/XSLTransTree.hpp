/**
 * @file
 * @version @$Id$
 * @author
 * ui-gxmlcpp: C++ wrapper for Gnome libxml2/libxsl.
 * Copyright (C) Schlund+Partner AG 2002-2004.
 * Licensed under the GNU Lesser General Public License (LGPL). See
 * @ref License, or the file COPYING in distribution.
 */

#ifndef UI_GXML_XSLTRANSTREE_HPP
#define UI_GXML_XSLTRANSTREE_HPP

// STDC++
#include <string>

// C++ Libraries
#include <ui-utilcpp/Text.hpp>
#include <ui-gxmlcpp/Tree.hpp>
#include <ui-gxmlcpp/XSLTree.hpp>

// Include compat w/o warnings or changing external UI_GXMLCPP_IGNORE_DEPRECATED
#ifdef UI_GXMLCPP_IGNORE_DEPRECATED
#include <ui-gxmlcpp/XMLTree.hpp>
#else
#define UI_GXMLCPP_IGNORE_DEPRECATED
#include <ui-gxmlcpp/XMLTree.hpp>
#undef UI_GXMLCPP_IGNORE_DEPRECATED
#endif

namespace UI {
namespace GXML {

/** @brief XSL translation result tree. */
class XSLTransTree: public Tree
{
public:
	/** @brief Error codes for exceptions. */
	enum ErrorCode
	{
		Mem_ = 0,
		Apply_
	};
	/** @brief Exceptions for this class. */
	typedef CodeException<ErrorCode> Exception;

	/** @brief Parameter abstraction for xsl translations. */
	class Params: private UI::Util::CStrArray
	{
	public:
		/** @brief Add a named parameter. */
		Params & add(std::string const & name, std::string const & value);
		using CStrArray::get;
	};

	/** @brief Constructor. */
	XSLTransTree(XSLTree const & xslTree, Tree const & tree, Params const & params=Params());

	/** @brief Compatibility constructor for old XMLTree. */
	XSLTransTree(XSLTree const & style, XMLTree const * const tree, Params const & params=Params());

	/** @brief See XSLTree::getOutputEncoding(). */
	std::string getOutputEncoding() const;

	/** @brief XSLTransTree serializer. */
	class Dump
	{
	public:
		/** @brief Constructor. */
		Dump(XSLTransTree const & xslTransTree);
		~Dump();

		/** @brief See XSLTree::getOutputEncoding(). */
		std::string getEncoding() const;

		/** @name Get serialized content.
		 * @{ */
		char const * getC() const;
		std::string get() const;
		int getSize() const;
		/** @} */
	private:
		XSLTransTree const * const xslTransTree_;
		xmlChar * buf_;
		int size_;
	};

protected:
	xsltStylesheet * getStylesheet() const;

private:
	XSLTree const * const xslTree_;
};

}}
#endif
