/**
 * @file
 * @version @$Id$
 * @author
 * ui-gxmlcpp: C++ wrapper for Gnome libxml2/libxsl.
 * Copyright (C) Schlund+Partner AG 2002-2004.
 * Licensed under the GNU Lesser General Public License (LGPL). See
 * @ref License, or the file COPYING in distribution.
 */
#ifndef UI_GXML_VALIDATORTREE_HPP
#define UI_GXML_VALIDATORTREE_HPP

// STDC++
#include <string>

// C++ Libraries
#include <ui-gxmlcpp/Tree.hpp>

namespace UI {
namespace GXML {

/** @brief Common abstract base class for validator trees (XML Schema and RelaxNG). */
class ValidatorTree: public Tree
{
public:
	/** @name Standard tree constructors.
	 * @{ @see UI::GXML::Tree. */
	ValidatorTree(char const * xml, int len=-1, std::string const & base=DefaultDocbase_, int const options=0);
	ValidatorTree(std::string const & xml, std::string const & base=DefaultDocbase_, int const options=0);
	ValidatorTree(std::istream & xml, std::string const & base=DefaultDocbase_, int const options=0);
	ValidatorTree(FileConstructor const dummy, std::string const & file, int const options=0);
	/** @} */

	virtual ~ValidatorTree();

private:
	/** @name Not implemented -- protect against segfaults.
	 * @{ */
	ValidatorTree(ValidatorTree const & tree);
	ValidatorTree & operator=(ValidatorTree const & tree);
	/** @} */

public:
	/** @brief Error codes for exceptions. */
	enum ErrorCode
	{
		Ctxt_=1,
		Parse_,
		ValidCtxt_,
		Internal_,
		TreeInvalid_
	};
	/** @brief Exceptions for this class. */
	typedef CodeException<ErrorCode> Exception;

private:
	virtual int libxml2Validate(Tree const & tree) const = 0;

public:
	/** @name Validate given trees.
	 * @{ */
	bool isValid(Tree const & tree) const;
	Tree const & validate(Tree const & tree) const;
	/** @} */
};

}}
#endif
