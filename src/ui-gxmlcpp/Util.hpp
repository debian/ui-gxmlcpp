/**
 * @file
 * @version @$Id$
 * @author
 * ui-gxmlcpp: C++ wrapper for Gnome libxml2/libxsl.
 * Copyright (C) Schlund+Partner AG 2002-2004.
 * Licensed under the GNU Lesser General Public License (LGPL). See
 * @ref License, or the file COPYING in distribution.
 */
#ifndef UI_GXML_UTIL_HPP
#define UI_GXML_UTIL_HPP

// STDC++
#include <cassert>

// C libraries
#include <libxml/globals.h>

// C++ libraries
#include <ui-utilcpp/Misc.hpp>

namespace UI {
namespace GXML {

/** @brief Control freeing of memory via xmlFree. */
template <typename P>
class auto_xmlfree: public UI::Util::auto_base<P>
{
public:
	/** @brief Control this memory allocated via std::*alloc. */
	auto_xmlfree(P * const p)
		:UI::Util::auto_base<P>(p)
	{}

	/** @brief Free memory via std::free. */
	virtual ~auto_xmlfree()
	{
		xmlFree(UI::Util::auto_base<P>::p_);
	}
};

}}
#endif
