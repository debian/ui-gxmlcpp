/**
 * @file
 * @version @$Id$
 * @author
 * ui-gxmlcpp: C++ wrapper for Gnome libxml2/libxsl.
 * Copyright (C) Schlund+Partner AG 2002-2004.
 * Licensed under the GNU Lesser General Public License (LGPL). See
 * @ref License, or the file COPYING in distribution.
 */
#ifndef UI_GXML_XPATHOBJECT_HPP
#define UI_GXML_XPATHOBJECT_HPP

// STDC++
#include <string>

// C libraries

// C++ libraries
#include <ui-gxmlcpp/Exception.hpp>
#include <ui-gxmlcpp/XPathContext.hpp>

namespace UI {
namespace GXML {

class Tree;

/** @brief XPath object holder class. */
class XPathObject: private UI::Util::auto_base<xmlXPathObject>
{
	/** @todo Compat. */
	friend class XMLTree;
	friend class Tree;

private:
	static xmlXPathObjectPtr create(xmlXPathContextPtr const context, std::string const & xpath);

public:
	/** @brief Error codes for exceptions. */
	enum ErrorCode
	{
		EvalError_,        // Error evaluation the xpath.
		NotAString_,       // Value is not a string
		NotABool_,         // Value is not a bool
		NotAFloat_         // Value is not a float
	};
	/** @brief Exceptions for this class. */
	typedef CodeException<ErrorCode> Exception;

protected:
	/** @brief Internal constructor. */
	XPathObject(xmlXPathContextPtr const context, std::string const & xpath);

public:
	/** @brief Public constructor. */
	XPathObject(XPathContext const & context, std::string const & xpath);

	~XPathObject();

public:
	/** @brief Abstract from libxml2 type. */
	typedef xmlXPathObjectType Type;
	/** @brief Get type of this xpath object. */
	Type getType() const;

	/** @name Get value of this xpath object.
	 *
	 * If this object's type is not of according type, these methods
	 * will throw an appr. exception.
	 *
	 * @{ */
	std::string getString() const;
	double getNumber() const;
	bool getBoolean() const;
	/** @} */

	/** We cannot declare friend to a nested class ;(. */
	using UI::Util::auto_base<xmlXPathObject>::get;
	using UI::Util::auto_base<xmlXPathObject>::operator->;

private:
	std::string const xpath_;
};

}}
#endif
