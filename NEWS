Summaries of important changes how ui-gxmlcpp is used or behaves.
See ChangeLog for more details.

Downloads:
  o Generic: http://sourceforge.net/projects/ui-gxmlcpp/files//ui-gxmlcpp
  o Current: http://sourceforge.net/projects/ui-gxmlcpp/files//ui-gxmlcpp/stable/ui-gxmlcpp-1.4.6.tar.gz
================================================================
ui-gxmlcpp-1.4.6 (stable) (Mon, 09 Jan 2023 11:14:51 +0100):

Patched stable release.

Changes since 1.4.5:

Stephan Sürken (14):
  * [b4cb1fe] Add .gitattributes file to easy merging from
    stable.
  * [cb39c95] .gitattributes: Add hint that --global also works
    for 'ours' merge drive (usually makes more sense).
  * [c9cf6c3] Doxyfile.in: Update w/ doxygen 1.9.4 (fixes
    warnings)
  * [1e75918] Doxyfile.in: Change PAPER_TYPE (a4wide->a4) (fixes
    doxygen warning)
  * [a0b10c5] src/tools/[Compat]UnitTests.cpp: Update deprecated
    boost unit test includes
  * [ab3e251] configure.ac: Update w/ ``autoupdate`` 2.71
  * [8cf265c] .ui-auto.conf: Update testbuild to sid+bullseye
    only
  * [e1f4b63] .ui-auto.conf: Update Debian git repo to salsa
  * [3727ef3] configure.ac: Copy version from former stable
    branch (will do maintenance from master only henceforth)
  * [27c33db] configure.ac: Prepare 1.4.6 (5:2:0)
  * [b40a587] NEWS: Copy from former stable branch
----------------------------------------------------------------
ui-gxmlcpp-1.4.5 (stable) (Tue, 03 Mar 2020 16:36:16 +0100):

Patched stable release.

Changes since 1.4.4:

Stephan Sürken (4):
  * [eda9e20] Doxyfile.in: Upgrade to 1.8.16.
  * [50d41cb] ui-gxmlcpp/Conf.hpp: Fix new doxygen warning.
  * [5bd0d81] configure.ac: Use pkg-config for libxml2 and
    libxslt (thanks to Hugh McMaster for the patches).
  * [b9e29cd] configure.ac: Prepare 1.4.5.
----------------------------------------------------------------
ui-gxmlcpp-1.4.4 (stable) (Mon, 08 Aug 2016 09:40:38 +0000):

Patched stable release.

Changes since 1.4.3:

Stephan Sürken (8):
  * [2829c5a] configure.ac: Push min-dep for ui-utilcpp to 1.8.3
    (LFS).
  * [a5e7bd2] tools/UnitTests.cpp: Add helper function:
    randomString().
  * [8adbc71] UnitTests.cpp: Add test: NodeDump with sizes ~>=
    4k.
  * [bb950f7] Buffer.cpp: Fix xmlOutputBuffer allocation (Fixes
    >4k "node dumps").
  * [fc8029c] Use 'unique_ptr' for C++11, else fall back to
    'auto_ptr'.
  * [7e78288] .ui-auto.conf: Fix for new '~SID' hellfield
    version restriction.
  * [1b55095] configure.ac: Prepare 1.4.4.
  * [eb84000] Switch to (re)use auto_ptr compat from ui-utilcpp.
----------------------------------------------------------------
ui-gxmlcpp-1.4.3 (stable) (Wed Jul 30 08:11:27 UTC 2014):

Patched stable release.

Initial release from SF, and fixes for new libxml2 >=2.9.

Changes:

Stephan Sürken (10):
  * [0a0144f] configure.ac: Add AC_SYS_LARGEFILE
  * [0785089] Add .gitignore file.
  * [d2a6339] compat-1.0: Apply FTBFS fixes from sp-gxmlcpp
    Debian package.
  * [d29f82b] Doxyfile.in: Update to 1.8.7; no longer produce
    RTF.
  * [37f01ca] README: Updates and fixes.
  * [10bd226] Makefile.am: Add .gitignore to dist.
  * [fcad742] .ui-auto.conf: Adapt to new setup (SF, alioth,
    mbd).
  * [04f5642] Buffer: Support libxml2 >= 2.9
    (LIBXML2_NEW_BUFFER)
  * [7e37eb1] COPYING: Update LGPL-2.1 text file to fix FSF
    postal address.
  * [fe53a9d] configure.ac: Prepare 1.4.3.

----------------------------------------------------------------
ui-gxmlcpp-1.4.2 (stable) (Thu Nov  5 17:54:48 UTC 2009):

Maintenance release; lots of code cleanups (warning fixes and
"synchronizing" the windows port).

Bug fixes:
  * Fix: Add new header compat_warning to avoid redundancies.
  * XSLTRansTree.hpp: Dont undef UI_GXMLCPP_IGNORE_DEPRECATED if it was def'ed before
----------------------------------------------------------------------
ui-gxmlcpp-1.4.1 (stable) (Tue Jun 23 09:38:05 UTC 2009):

This new stable only has some updates for new ui-auto in
.ui-auto.conf.
----------------------------------------------------------------------
ui-gxmlcpp-1.4.0 (stable) (Fri Mar 27 14:31:06 UTC 2009):

This is a new stable version, updating to ui-utilcpp 1.4 and
some minor fixes. SO main version dumped to 5 (stable).

Changes since 1.3.1 (unstable):

  * Update tar archive format to tar-pax.
----------------------------------------------------------------------
ui-gxmlcpp-1.3.1 (unstable) (Mon Jan 19 16:05:42 UTC 2009):

Minor unstable update release:

  * Release with new ui-auto; adds missing manpage for ui-gxmlcpp-version.
  * Doxyfile.in: Fix EXAMPLE_PATH to be generated dynamically, not "./".
----------------------------------------------------------------------
ui-gxmlcpp-1.3.0 (unstable) (Thu Dec  4 16:52:22 UTC 2008):

Initial unstable 1.3 release:

  * Bug Fix: Validator trees (RelaxNG+Schema): "validator Context" was used as if
    thread-safe, but it is not.
  * Depend on ui-utilcpp 1.3 (boost 1.35 update).
  * Main library version dumped to 4 (unstable).
  * Update doxygen config to 1.5.6.
----------------------------------------------------------------------
ui-gxmlcpp-1.2.0 (stable) (Thu Jun 19 12:28:52 UTC 2008):

Stable release 1.2. Bump library main version to 3 (stable).

  * Update configure check for utilcpp to 1.2 (stable).
  * sp2ui: Fix: Also update "SP_GXMLCPP" to "UI_GXMLCPP" (some macros affected).
----------------------------------------------------------------------
ui-gxmlcpp-1.1.18 (unstable) (Tue Jan  8 17:55:55 UTC 2008):

  * boost test: Boost 34 deprecated some parts of API; updated to
    a variant that works for both, 33 and 34.
  * Tested against upcoming g++-3.4 and fixed all compile errors and
    warnings.
  * README: Remove/update various obsolete/outdated comments.
----------------------------------------------------------------------
ui-gxmlcpp-1.1.17 (unstable) (Mon Jan  7 16:59:23 UTC 2008):

Initial "ui" release:
  * Update to ui-auto.
  * Update to ui-utilcpp.
  * Update _all_ "sp" prefixes to "ui" (macro names, file names, C++ namespaces).
  * Add tool ui-gxmlcpp-sp2ui to automatically update projects using us.
----------------------------------------------------------------------
sp-gxmlcpp-1.1.16 (unstable) (Fri Aug 24 11:01:04 UTC 2007):

It fixes the somewhat hidden necessity that different types of XML
(most importantly XSL) need certain options flags set to the (generic)
XML parser, else a segfault might occur when using other
functionality later ;).

Mandatory parse options are now always set in the respective class;
also, more (libxml2) parse options can now be set by the use for any
Tree* class manually.

This release has API changes (this is still "unstable" ;); dependent
projects must recompile. 

  * All *Tree classes: Add optional "parse options" arg to all std constructors.
  * Add mandatory options:
    - Tree (all): XML_PARSE_NOBLANKS
    - XSLTree   : XSLT_PARSE_OPTIONS
----------------------------------------------------------------------
sp-gxmlcpp-1.1.15 (unstable) (Tue Mar 13 09:38:01 UTC 2007):

  * Fix: Forgotten "\" left "Shortcuts.hpp" uninstalled ;(.
----------------------------------------------------------------------
sp-gxmlcpp-1.1.14 (unstable) (Mon Mar 12 14:55:35 UTC 2007):

  * Initial etch-based release (automake 1.10, sp-auto 103).
  * Add convenience header sp-gxmlcpp/Shortcuts.hpp.
  * Having the SP_PROG_CXX c++ checker (rules out g++-2.95): Removed
    all gcc 2.95 compat code.
----------------------------------------------------------------------
sp-gxmlcpp-1.1.13 (unstable) (Fri Nov  3 14:07:38 UTC 2006):

Updates for sp-auto 0.2 only.
----------------------------------------------------------------------
sp-gxmlcpp-1.1.12 (unstable) (Mon Aug 28 14:32:39 UTC 2006):

  * "Relative xpath" support for nodes (new methods Tree::Node::getNode[Set]).
----------------------------------------------------------------------
sp-gxmlcpp-1.1.11 (unstable) (Mon Aug 14 11:20:49 UTC 2006):

  * Add "hasAttribute()" helper for nodes.
  * Add name matching support for all "NodeSet-acquiring" methods.
----------------------------------------------------------------------
sp-gxmlcpp-1.1.10 (unstable) (Thu Jul 13 14:06:01 UTC 2006):

  * Some minor cleanups for exceptions.
  * Add ValidatorTree as common abstract class for RelaxNG+SchemaTree,
    to share the common code
  * Validator(=Relax+Schema)Tree API change: "validate" replaced by
    "isValid"; "validate" now throws "TreeInvalid_" on validation error
    and returns tree ref on success.
----------------------------------------------------------------------
sp-gxmlcpp-1.1.9 (unstable) (Fri Apr 21 09:41:01 UTC 2006):

  * Added proper support for namespaces in XPath expressions.
----------------------------------------------------------------------
sp-gxmlcpp-1.1.8 (unstable) (Wed Jan 25 18:29:16 UTC 2006):

  * Log: Replacing one strange/possibly dangerous usage of string
    constructor w/ explicit resize().
----------------------------------------------------------------------
sp-gxmlcpp-1.1.7 (unstable) (Mon Dec  5 12:55:40 UTC 2005):

This release avoids segfaults on wrong usage, and adds needed =/default
copy for Tree.

  * XSL/RelaxNG/SchemaTree: Protect "=", Tree(Tree) to avoid wrong usage.
  * Tree:
    - Implement operator= (this fixes free segfault when default copy operator is used).
    - Implement Tree(Tree) constructor (this fixes free segfault when default copy constructor is used).
    - Implement operator== (for convenience).
    - Protect Tree() (default constructor should not be used).
  * Using EmptyString_ from sp-utilcpp.
----------------------------------------------------------------------
sp-gxmlcpp-1.1.6 (unstable) (Fri Jun 17 15:26:11 UTC 2005):

  * Update to sp-auto >= 0.1.1 (fixes serious cruft-leftover bug in
    tarball).
----------------------------------------------------------------------
sp-gxmlcpp-1.1.5 (unstable) (Fri Jun 17 12:40:32 UTC 2005):

  * Initial release using sp-auto.
  * Big update to doxygen docs, removing all doxygen warnings.
  * "g++-3.4 -pedantic"able (nightly test now lethal).
----------------------------------------------------------------------
sp-gxmlcpp-1.1.4 (Tue Feb 15 15:26:49 UTC 2005):

This is the unstable relase series 1.1.x. DON'T USE THIS for any
PRODUCTION code. Purpose of this release is to provide an easy way to
TEST it and ideally report bugs.

   - XSLTransTree: Parameter support for xsl translation.
   - Cleaning and repairing dumps:
     - Generic dump options for all dumps: format, encoding [, nodeSeparator]
     - Removing "child" option from all dumps.
     - Removing obsolete formatDumpDump.
     [ This also fixes the the encoding problem for node dumps. ]
   - Fix: Complete tree dumps with proper XML header: Make them have encoding always.
   - All addChild() methods: Returning the new node.
   - Adding setTopLevelNamespace() configurator to Tree. Seemingly, this
     is still needed ;(.
   - XSL trans and dumping: Segfault fixes: 
     - XSLTrees with encoding being NULL.
     - XSLTransTree::Dump for empty translation results (when
       xslToString writes 0 bytes, even buf_ pointer stays NULL).
   - Tree: add xsltDump tool function.
   - NodeSet: unlink support.
   - "child" support for NodeSet::Dump. Obsolete/Redundant Node::Dump
     class removed.
   - Bugfix: getCount() in compat interface.
   - Bugfix: Missing include "Util.hpp".
   - Tree: Parsing with NO_BLANKS by default.
   - Add Tree::formatDumpDump tool method.
   - Fix error handling for modifying Tree methods:
     - getNode() now throws if needed.
     - All modifying methods updated to enable throw.
   - Tree::dump() fix: Running formatDump with format=false for "/".
   - Conf: Add setKeepBlanks option, setting false as default (this is
     a reasonable default. It keeps DOM trees as compact as possible and
     make format dumps work as expected.)
   - Conf: Add prefix handling (log cosmetics).
----------------------------------------------------------------------
sp-gxmlcpp-1.1.3 (Mon Aug 30 12:39:25 UTC 2004):

This is the unstable relase series 1.1.x. DON'T USE THIS for any
PRODUCTION code. Purpose of this release is to provide an easy way to
TEST it and ideally report bugs.

	- Fix circular dependencies caveat. We will now have only _one_ library
    to link against (src-compat-1.0 moved to src/compat-1.0).
  - Proper solution for hacky chmod +x *-version.
  - Use SP_GXML_IGNORE_DEPRECATED diversed, in source only where needed
    (except for the compat library itself).
  - Removing XMLObjectWrapper, using SP::Util::auto_base (private) natively.
    This also fixes some "libxml/xslt direct access via get()" issues.
  - Number of upgrade hints added to README.
  - Tree::Node: Using NullNode, adding getChild(), getParent(), etc. for
    easy navigating.
  - Tree, TreeNode, Tree::NodeSet: Quite a number of shortcuts/handy tools
    added.
  - Merging .strap_run/.strap_dev into ".strap_dev [runonly|ggcenv]".
    - Use ./configure to produce that file. It can now be sourced
      properly (no need for backticks).
    - Default bevaviour now is to taint autotool-supported variables
      only. This should always work if your project using this library
      alos uses autotools.
    - "gccenv" sets the gcc environment variables as before. autotools
      test may fail unexpectedly.
  - Protecting all enum values from preprocessor by naming convention
    ("CatDog_" like for constant class variables).
  - Update to automake 1.9, using "tar-ustar < 155" as archive
    format. This fixes both, the "filename too long" and the "make dist
    keeps silent" bug. Warn about libtool <1.5.8 in strap_auto.
----------------------------------------------------------------------
sp-gxmlcpp-1.1.2 (Fri Aug 13 13:46:11 UTC 2004):

This is the unstable relase series 1.1.x. DON'T USE THIS for any
PRODUCTION code. Purpose of this release is to provide an easy way to
TEST it and ideally report bugs.

  - Addded library version checking scheme (see sp-utilcpp NEWS for details).
  - Checking and actually _using_ sp-utilcpp. This got rid of all redundancies,
    and gets Exceptions setup right .
----------------------------------------------------------------------
sp-gxmlcpp-1.1.1 (Mon Jul  5 14:22:31 UTC 2004):

This is the unstable relase series 1.1.x. DON'T USE THIS for any
PRODUCTION code. Purpose of this release is to provide an easy way to
TEST it and ideally report bugs.

Preliminary summary for 1.2.x:

This is a severe rewrite of the 1.0.x interface, with less code, more
features and exception safety.  It relies on newer versions of libxml2
(>=2.6.10) and libxslt (>= 1.1.7).

Compatibility is retained for old classes XMLTree, XMLDump, XMLNode,
XMLNodeSet, XSLTrans (leave alone exception handling, which is fixed
even there).

The new interface mainly consists of the collaborating classes
XPathObject, XPathContext, Tree (Tree::Node, Tree::NodeSet), XSLTree
and XSLTransTree (this now holds xsl translation results
only). Serializer (XXX::Dump) classes are added where appropriate,
replacing the functionality of the old XMLDump. SchemaTree and
RelaxNGTree can be used to validate any other trees.

Some other new features include;

  - Tree: Support for "multi- or monocontext trees" (for multi- or
    monothreading, resp.)
  - Conf: Global configuration/custom logging support (class Conf).
  - autotools cleanup; local libxslt check macro. We check now for
    libxml2 2.6.10 and libxslt 1.1.7 min version. All compat for
    earlier versions removed.

More (unstructured and incomplete) changes may be found in the
"sp-gxmlcpp-1.1.0 (NEVER RELEASED)"-NEWS below.
----------------------------------------------------------------------
sp-gxmlcpp-1.1.0 (NEVER RELEASED):

  * More straighforward directory layout, putting all C++-Source under
    src/: sp-gxmlcpp (the library) and examples (Example applications).
  * DESTDIR support for local install targets.
	* Update to new exception scheme:
    - Exception superclass: SP::GXML::Exception
      In short: std::exception interface, handling for an error
      description and a debug information string. Special support for
      errno handling provided.
    - SP::GXML::CodeException: SP::GXML::Exception template adding the simple facility to add an
      (arbitrary) error code to SP::GXML::Exception.
    => Anything in namespace SP::GXML does
      now throw SP::GXML::Exception only (usual exceptions apply for
      indirect throws; ideally, these are catched down internally and
      encapsulated).
    - Updated all classes to have a subclass "Exception". Classes that used to throw
      enums are now throwing "CodeException<ErrorCode>", with ErrorCode being the old
      enum-type exceptions, and the subclass "Exception" being a typedef to that.
    - 4 basic macros provided to provide for automatic file name and line no debugging.

    => An "Exception" for every context, enabling diversed or generic
    catches, automatic errno errors and file name/line no debugging.
  * Code/style review of all files:
    - Generic class variable naming convention ("_myName").
    - Many const correctness updates.
    - Removing all "one line scopes".
  * XMLTree / XPathContext problem:
    - Making XPathContext class public subclass of XMLTree.
    - All (two) internal XPathContext class copies replaced.
    - Adding two more holder classes: Doc (xmlDoc) and XPathObject (xmlXpathObject).
    => Being exception safe, memory leak wise.

  * Uhm. Fullstop. This hoovercraft is full of eel. No real
    abstraction and redundancies all over the place.

    Deciding to do a more thorough rewrite (at least C++/object reuse
    wise; same functionality; don't know too much about XML/DOM to improve
    that):
    - Class brainstorm I. libxml2 structures. Afaics, we need clean
      encapsulations for the following libxml2 structures:

      - xmlNode (class "Node") -- copied from XMLNode.
      - xmlDoc ("Doc") -- created.
      - xmlXPathContext ("XPathContext") -- created.
      - xmlXPathObject ("XPathObject") -- created.

      The namespace is "GXML" = Gnome XML. No need for a "XML" prefix
      for these classes.
 
    - Class brainstorm II. libxslt structures. Afaics, we need clean
      encapsulations for the following libxml2 structures:

    TOO MANY CHANGES. See documentation for the new Interfaces; it's a
    rewrite. Pseudo release 1.1.0.
----------------------------------------------------------------------
